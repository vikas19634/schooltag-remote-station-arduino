# School Tag Remote Station

Remote stations are mounted on posts around town along the Safe Route to School.  
When students see a station they hold their NFC School Tag up to the station to 
make a record of the visit.  The station transfers a record of the station visit
into the tag.

When the student arrives at school, the student tags in at the School Station which
reads all of the visits made and rewards the student based on the rules of the game.

## Arduino 

This project will host the Arduino implementation of the remote station. 

note: This program is not exact implementation of schooltag remote particle station for now.           
It just reads/writes given data to Ntag213 for basic test on arduino platform.



## See Also

[Particle Remote Station](https://bitbucket.org/school-tag/school-tag-remote-station-particle/)
[Particle School Station](https://bitbucket.org/school-tag/school-tag-station-particle/)